<!-- Purpose: to troubleshoot issues with Digital Marketing campaigns, tracking, performance, or strategy. -->

## Problem Statement
What is the problem that we should look into? What is the result of the current problem and potential resolution and impact?

## Hypothesis
* 
* 

## Where to Investigate?

**Systems or Channel:**
- [ ] Sisense Dashboard
- [ ] Alli Dashboard (from PMG Agency)
- [ ] Marketo
- [ ] Salesforce
- [ ] LinkedIn
- [ ] Facebook
- [ ] Paid Search
- [ ] Paid Display
- [ ] Publishers


## Links
* Add relevant links 


/label ~"Digital Marketing Programs" ~"dg-digitalmarketing" ~"mktg-status::triage" ~"DG-Priority::ToBePrioritized"
/confidential
/assign @zbadgley
