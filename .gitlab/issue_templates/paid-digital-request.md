<!-- Digital Marketing Promotion Requests
If you are requesting digital marketing support for a campaign, event, webcast, new whitepaper, paid sponsorship, display ads, syndication, etc, please fill out the details below. We need at least 2 weeks to prepare most display advertising, paid search, and/or paid social campaigns because of ad design and agency turnaround time. -->

<!-- Please provide necessary information not included in related issue/epic in order to launch a digital campaign. If you don't know all the details, fill in as much as you can: -->

### Digital Campaign Brief
- Link to related Issue/Epic: `Enter URL`
   - If no related Issue/Epic, please make a copy and fill out [Campaign Brief](https://docs.google.com/document/d/1Ba6jBhaJp-2TmAhr7HKDzQVGyNLO3D-t7TV4Wk21RlU/edit?usp=sharing): `Enter URL of campaign brief`
- Campaign Name: 
- Campaign Budget for All Digital Tactics:
- Campaign Allocadia ID: 
- Team Requesting Promotion: 
    - If the requesting team is outside of Digital/Demand Generation, you will need to provide all details in this campaign request so the Digital Marketing team can generate a statement of work from our agency. Digital will then share the SOW here and you will need to submit a Coupa requisition in order to run this campaign. Please follow the instructions outlined [here](https://about.gitlab.com/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/#po-process).
- Campaign Description: 
- Campaign Start and End Dates: 
- Campaign Goal: `Registrations, page views, downloads, etc`
- Landing Page URL(s) to Use: `Please provide copy if LP is not live yet`
- Campaign Creative Asset: `Upload to issue, enter URL(s) to GitLab creative asset(s), or link to the design request issue`

<!-- - This may require a separate ask for Design team, please make request in Corporate project and factor into timeline
   - [Here](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#digital-campaign-design-specs) is a reference sheet for digital campagin design specifications.
   - Search [creative repository](https://gitlab.com/gitlab-com/marketing/corporate-marketing/tree/master/design) if you want to use existing creative
- Note: We require a 15% agency fee for all budgets. See how your total budget is broken down with the [Budget Breakdown Calculator](https://docs.google.com/spreadsheets/d/1FIFD-wO8Ib9yHyRBnfMCxsdyHYoJtd4Dw3PJugKKgkQ/edit#gid=0)
- Note: If you need recommendations on budget, please use the [Budget Recommendation Calculator](https://docs.google.com/spreadsheets/d/1FIFD-wO8Ib9yHyRBnfMCxsdyHYoJtd4Dw3PJugKKgkQ/edit#gid=1509409312) -->

### Targeting

<!-- DMPs will pull targeting criteria from related issue/epic.
Need help with targeting ideas? Review this [list of targeting options](https://docs.google.com/spreadsheets/d/1ZuzFj-R7uPng76hI3XFx0kclR_uFIY5hcg0ikTZk4KM/edit?usp=sharing). -->

- Named Account List: `Enter link to target account list (preferably already downloaded to a spreadsheet)`
- Geo-location(s) to target (if any): `Enter geo-location(s) to target (i.e. regions, sub-regions, countries, etc.)`
- Additional Targeting: `If more granular targeting is preferred, list here if not already in related issue/epic`


### Digital Campaign Types
<!-- Please select your preference, or leave blank for DMP recommendation. Learn more about each campaign type [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#program-definitions). -->
- [ ] LinkedIn Ads
   - If using Lead Gen Forms, please create a new Marketo program & SFDC campaign per [instructions here](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#steps-to-setup-linkedin-lead-gen-form-gated-content-only), or add this new asset(s) to existing program(s) under the correct integrated campaign ([list here](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/MF6504A1) under `Social Media Content`).
- [ ] LinkedIn InMail Ads
   - Add the preferred sender’s LinkedIn Profile here: `LinkedIn profile URL`
   - InMail message template will be available upon request
- [ ] Facebook Ads
- [ ] Paid Sponsorship
- [ ] Paid Search
- [ ] Paid Display - If account-targeted paid display, please open separate [Demandbase campaign request issue ](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/blob/master/.gitlab/issue_templates/Demandbase_Campaign_Request.md)
- [ ] Content Syndication - Please also create a [`Content Syndication Epic`](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication/)


### Campaign Reporting (DMP to fill in)
- Link to report: 
- Final spend: 
- Summary: 
- Additional notes: 


<!-- Please do not edit below -->

/assign @zbadgley @laurenroberts

/label ~"Digital Marketing Programs"
/label ~"mktg-status::triage" 
