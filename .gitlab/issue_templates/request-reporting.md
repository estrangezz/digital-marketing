<!-- Purpose of this issue: To pull specific set(s) of data from Digital Marketing reporting dashboards.-->

## Overview
Describe the reporting being requested.

## Purpose
What goal are you trying to achieve with the requested reporting and what impact will this make?

## Links
* 

## Timeline
When do you need the data by? Please link any related issues that this reporting issue is blocking.

/label ~"Digital Marketing Programs" ~"dg-digitalmarketing" ~"mktg-status::triage" ~"DG-Priority::ToBePrioritized"
/confidential
/assign @zbadgley
