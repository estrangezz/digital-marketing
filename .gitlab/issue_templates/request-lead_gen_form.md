<!-- Please use this template only if you are requesting a paid social campaign that includes lead gen forms (LGFs). -->

### Digital Campaign Info
- Link to campaign request issue: `Enter URL`
   - If no issue yet, please link the epic: `Enter URL`
- Campaign Name: 
- Campaign Segment: `ENT, MM, or SMB`
- Campaign Region: `AMER, EMEA, APAC, or Global`
- Campaign Sub-Region: 
- Campaign Language: 
- Campaign Asset(s) Name: 
- Campaign Targeting: `prospecting, retargeting, account targeting, or account retargeting`
- Campaign GTM/Use Case: `CI/CD, GitOps, DevOps GTM, DevSecOps`
- Landing Page URL(s):
   - If using direct lead gen form (sending users directly to content): `Enter ungated URL`
   - If using autoresponder lead gen forms (sending users an email with asset): `Enter URL of page to explore while requested content is being sent via email`

### Set-up Checklist

- [ ] **Digital Marketing:** Deliver Campaign Info to PMG to create LGFs in platform
   - If this is a localized campaign, secure additional LGF copy and privacy policy notice translations from the campaign requester.
- [ ] **Digital Marketing:** Provide LGF name to Campaign Manager
- [ ] **Campaign Manager:** Review asset categorization & set-up program tracking accordingly:
   - If the asset(s) is categorized under an existing GTM Marketo program/SFDC campaign ([list here](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/MF6504A1) under `Social Media Content`), then add the new asset(s) to the program tokens and LGF name(s) to the Smartlist.
   - If the asset requires a new Marketo program & SFDC campaign, please set up following [these instructions](https://about.gitlab.com/handbook/marketing/marketing-operations/campaigns-and-programs/#steps-to-setup-linkedin-lead-gen-form-gated-content-only) with the Digital Campaign Info provided above.
- [ ] **Digital Marketing:** Submit a test lead
- [ ] **Campaign Manager:** Confirm test lead tracking correctly

### Completed Items

- [ ] Lead Gen Form name(s): 
- [ ] Marketo program: 
- [ ] SFDC campaign: 


<!-- Please do not edit below -->

/label ~"Digital Marketing Programs" ~"mktg-status::plan" ~"dg-priority::ToBePrioritized"

