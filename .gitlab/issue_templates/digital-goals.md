<!-- Purpose: for use by Digital Marketing to document monthly, quarterly, or annual OKRs, targets & metrics. -->
<!-- This issue type is ONLY to be opened by members of the Digital Marketing Team. -->

## Digital Marketing OKRs
<!-- List of major targets/goals/metrics set within a specified timeframe, explaining the "why" behind these items & what Digital Marketing is trying to achieve with them.  -->
*
*
*

## Tactics
<!-- List of initiatives that will be implemented to enable goals and/or support goal progression. -->


## Results
<!-- To be filled out after goal timeframe has passed. -->



/label ~"Digital Marketing Programs" ~"dg-digitalmarketing" ~"mktg-status::triage"  ~"DG-Priority::ToBePrioritized"
/confidential
